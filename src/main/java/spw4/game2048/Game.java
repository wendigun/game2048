package spw4.game2048;

import java.util.Random;

public class Game {

    private int[][] board;
    private int score;
    private int moves;
    public Game() {
        board = new int[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                board[i][j] = 0;
            }
        }
        moves = 0;
        score = 0;
    }
    public int getMoves(){ return moves; }
    public int getScore() { return score; }
    public int getValueAt(int x, int y) { return board[x][y]; }
    public boolean isOver() {
        boolean full = true;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                full = !(board[i][j] == 0);
                break;
            }
        }
        boolean pairFound = false;
        if (full) {                      // compare with neighbours
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (i < 3) {
                        pairFound = board[i][j] == board[i + 1][j];
                    }
                    if (j < 3) {
                        pairFound = board[i][j] == board[i][j + 1];
                    }
                    if (i != 0) {
                        pairFound = board[i][j] == board[i - 1][j];
                    }
                    if (j != 0) {
                        pairFound = board[i][j] == board[i][j - 1];
                    }
                    if (pairFound) {
                        break;
                    }
                }
            }
            return !pairFound;
        }
        return full;
    }

    public boolean isWon() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                    if(board[i][j] == 2048) {
                        return true;
                };
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result = result + "   " + String.valueOf(board[i][j]);
            }
            result = result + "\n";
        }
        return result;
    }

    public void initialize() {
        int column = new Random().nextInt(4);
        int row = new Random().nextInt(4);
        int[] probability = {2, 2, 2, 2, 4, 2, 2, 2, 2, 2};
        int value = probability[new Random().nextInt(10)];
        board[column][row] = value;
        score = 0;
        moves = 0;
        int coltmp = column;
        int rowtmp = row;

        while (coltmp == column && rowtmp == row) {
            column = new Random().nextInt(4);
            row = new Random().nextInt(4);
        }
        value = probability[new Random().nextInt(10)];
        board[column][row] = value;
    }

    private int[] prepareLine(int[] line){
        // tighten content:
        for (int i = 0; i < 4; i++){
            if (line[i] == 0) {
                for (int x = i; x < 3; x++){
                    line[x] = line[x + 1];
                }
                line[3] = 0;
            }
        }
        // merge pairs:
        for (int i = 0; i < 3; i++){
            if (line[i] == line[i+1]) {
                line[i] = line[i]*2;
                line[i+1] = 0;
                score = score + line[i];
            }
        }
        // tighten content again:
        for (int i = 0; i < 4; i++){
            if (line[i] == 0) {
                for (int x = i; x < 3; x++){
                    line[x] = line[x + 1];
                }
                line[3] = 0;
            }
        }

        return line;
    }

    public void move(Direction direction) {

        if (direction == Direction.up) {
            int[] process = new int[4];
            for (int j = 0; j < 4; j++) {
                for (int i = 0; i < 4; i++) {
                    process[i] = board[i][j];
                }
                process = prepareLine(process);
                for (int i = 0; i < 4; i++) {
                    board[i][j] = process[i];
                }
            }
        }

        if (direction == Direction.down) {
            int[] process = new int[4];
            for (int j = 0; j < 4; j++) {
                for (int i = 0; i < 4; i++) {
                    process[3 - i] = board[i][j];
                }
                process = prepareLine(process);
                for (int i = 0; i < 4; i++) {
                    board[i][j] = process[3 - i];
                }
            }
        }

        if (direction == Direction.left) {
            int[] process = new int[4];
            for (int j = 0; j < 4; j++) {
                for (int i = 0; i < 4; i++) {
                    process[i] = board[j][i];
                }
                process = prepareLine(process);
                for (int i = 0; i < 4; i++) {
                    board[j][i] = process[i];
                }
            }
        }

        if (direction == Direction.right) {
            int[] process = new int[4];
            for (int j = 0; j < 4; j++) {
                for (int i = 0; i < 4; i++) {
                    process[3 - i] = board[j][i];
                }
                process = prepareLine(process);
                for (int i = 0; i < 4; i++) {
                    board[j][i] = process[3 - i];
                }
            }
        }
        moves++;
    }

    public void setBoard(int[][] wantedBoard){
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                board[i][j] = wantedBoard[i][j];
            }
        }
    }

    public int[][] getBoard(){
        return board;
    }

}
