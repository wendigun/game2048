package spw4.game2048;

import org.junit.jupiter.api.*;
import spw4.game2048.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameTests {
    Game testGame;

    @BeforeEach
    void setupSUT() {
        testGame = new Game();
        int[][] board = {
                {8, 4, 0, 0},
                {4, 0, 0, 0},
                {2, 0, 2, 0},
                {2, 0, 4, 0},};
        testGame.setBoard(board);
    }

    @Test
    void testToString(){
        String tmp = "   8   4   0   0\n" +
                     "   4   0   0   0\n" +
                     "   2   0   2   0\n" +
                     "   2   0   4   0\n";
        System.out.println(tmp);
        System.out.println(testGame);
        Assertions.assertEquals(tmp, testGame.toString());
    }

    @Test
    void testInitialize() {
        Game initTest = new Game();
        initTest.initialize();
        int[][] tmp = initTest.getBoard();
        int count = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if(tmp[i][j] != 0){count++;}
            }
        }
       Assertions.assertTrue(count == 2);
    }

    @Test
    void testMoveDown(){
        System.out.println();
        System.out.println("testMoveDown");
        testGame.move(Direction.down);
        String tmp = "   0   0   0   0\n" +
                     "   8   0   0   0\n" +
                     "   4   0   2   0\n" +
                     "   4   4   4   0\n";
        System.out.println(tmp);
        System.out.println(testGame);
        Assertions.assertEquals(tmp, testGame.toString());
    }

    @Test
    void testMoveleft(){
        testGame.move(Direction.left);
        System.out.println();
        System.out.println("testMoveUp");
        String tmp = "   8   4   0   0\n" +
                     "   4   0   0   0\n" +
                     "   4   0   0   0\n" +
                     "   2   4   0   0\n";
        System.out.println(tmp);
        System.out.println(testGame);
        Assertions.assertEquals(tmp, testGame.toString());
    }

    @Test
    void testMoveright(){
        testGame.move(Direction.right);
        System.out.println();
        System.out.println("testMoveRight");
        String tmp = "   0   0   8   4\n" +
                     "   0   0   0   4\n" +
                     "   0   0   0   4\n" +
                     "   0   0   2   4\n";
        System.out.println(tmp);
        System.out.println(testGame);
        Assertions.assertEquals(tmp, testGame.toString());
    }

    @Test
    void testMoveup(){
        testGame.move(Direction.up);
        System.out.println();
        System.out.println("testMoveUp");
        String tmp = "   8   4   2   0\n" +
                     "   4   0   4   0\n" +
                     "   4   0   0   0\n" +
                     "   0   0   0   0\n";
        System.out.println(tmp);
        System.out.println(testGame);
        Assertions.assertEquals(tmp, testGame.toString());
    }

    @Test
    void testMovedown(){
        testGame.move(Direction.down);
        System.out.println();
        System.out.println("testMovedown");
        String tmp = "   0   0   0   0\n" +
                     "   8   0   0   0\n" +
                     "   4   0   2   0\n" +
                     "   4   4   4   0\n";
        System.out.println(tmp);
        System.out.println(testGame);
        Assertions.assertEquals(tmp, testGame.toString());
    }

    @Test
    void testIsWon() {
        Game winnerGame = new Game();
        int[][] board = {
                {8, 4, 0, 0},
                {4, 0, 0, 0},
                {2, 0, 2048, 0},
                {2, 0, 4, 0},};
        winnerGame.setBoard(board);
        Assertions.assertTrue(winnerGame.isWon());
    }

    @Test
    void testGetScore(){
        int score = testGame.getScore();
        Assertions.assertEquals(score, 0);
    }

    @Test
    void testIsOverWith0() {
        Assertions.assertFalse(testGame.isOver());
    }

    @Test
    void testIsOverWithOut0() {
        Game winnerGame = new Game();
        int[][] board = {
                {8, 4, 2, 8},
                {4, 4, 4, 16},
                {2, 4, 2048,160},
                {2, 2, 4, 8},};
        winnerGame.setBoard(board);
        Assertions.assertFalse(winnerGame.isOver());
    }

    @Test
    void testIsOverReallyOver() {
        Game winnerGame = new Game();
        int[][] board = {
                {8, 4, 8, 4},
                {4, 8, 4, 8},
                {8, 4, 8, 4},
                {4, 8, 4, 8},};
        winnerGame.setBoard(board);
        Assertions.assertTrue(winnerGame.isOver());
    }
}
